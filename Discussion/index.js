// Express server

const express = require('express');
const taskRoutes = require('./routes/taskRoutes')


const port = 4000; 
const app = express(); 

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// feedback when the port is running
app.listen(port, () => console.log(`Server is running at port ${port}`))



const mongoose = require('mongoose'); // For connecting MongoDB

mongoose.connect("mongodb+srv://iamxdv:zuittb145@cluster0.bibta.mongodb.net/Session32?retryWrites=true&w=majority", 
        {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }
);

// Tho this is not required but for the status clarity 
let db = mongoose.connection;


db.on("error", console.error.bind(console, "There is an error with the connection"));
db.once("open", () => console.log("Successfully connected to the database"));


app.use('/tasks', taskRoutes)

/* 
==================A C T I V I T Y============================
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the /tasks/:id route using postman to get a
specific task.
5. Create a route for changing the status of a task to complete.
6. Create a controller function for changing the status of a task to
complete.
7.
Return the result back to the client/Postman.
8. Process a PUT request at the /tasks/:id/complete route using
postman to update a task.
9. Create a git repository named $33.
10. Initialize a local git repository, add the remote link and push to git with
the commit message of Add activity code.
11. Add the link in Boodle.

==============================END================================
*/