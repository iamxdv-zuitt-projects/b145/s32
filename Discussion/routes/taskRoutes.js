const express = require('express');
const router = express.Router();
const taskControllers = require('../controllers/taskControllers');

// retrieving
router.get("/", (req, res) => {
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController));
});

/*
	Mini-Activity
	1. Create a route for createTask controller, name your endpoint as /createTask
	2. Send your postman screenshot in hangouts
	ENDS IN 10 MINUTES- 7:55 PM
*/
// SOLUTION:
router.post('/createTask', (req, res) => {
	taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

// END OF SOLUTION

// Deleting
router.delete("/deleteTask/:id", (req, res) => {
	taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// Updating
router.put("/updateTask/:id", (req, res) => {
	taskControllers.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})
// =====================END OF DISCUSSION==========================

/* 
==================A C T I V I T Y============================
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the /tasks/:id route using postman to get a
specific task.
5. Create a route for changing the status of a task to complete.
6. Create a controller function for changing the status of a task to
complete.
7.
Return the result back to the client/Postman.
8. Process a PUT request at the /tasks/:id/complete route using
postman to update a task.
9. Create a git repository named $33.
10. Initialize a local git repository, add the remote link and push to git with
the commit message of Add activity code.
11. Add the link in Boodle.

==============================END================================
*/

// ======================ACTIVITY SOLUTION=================================
router.get("/:id", (req, res) => {
	taskControllers.specificTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

router.put("/:id/complete", (req, res) => {
	taskControllers.updateSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController))
})
// ======================END OF ACTIVITY SOLUTION============================

module.exports = router;
