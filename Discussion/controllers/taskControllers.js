const Task = require('../models/taskSchema');

// Retrieving all tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	});
};

// Creating a task
module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
		
	});

	return newTask.save().then((task, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return task
		}
	})
}

// Delete Task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}

// Update Task

module.exports.updateTask = (taskId, reqBody) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		} 

		result.name = reqBody.name
			return result.save().then((updatedTask, saveErr) => {
				if(saveErr){
					console.log(saveErr)
					return false
				} else {
					return updatedTask
				}
			})
	})
}

/* 
==================A C T I V I T Y============================
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the /tasks/:id route using postman to get a
specific task.
5. Create a route for changing the status of a task to complete.
6. Create a controller function for changing the status of a task to
complete.
7.
Return the result back to the client/Postman.
8. Process a PUT request at the /tasks/:id/complete route using
postman to update a task.
9. Create a git repository named $33.
10. Initialize a local git repository, add the remote link and push to git with
the commit message of Add activity code.
11. Add the link in Boodle.

==============================END================================
*/

// ======================ACTIVITY SOLUTION=================================
module.exports.specificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result
	});
};

module.exports.updateSpecificTask = (taskId, reqBody) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		} 

		result.status = "completed"
			return result.save().then((updatedSpecificTask, saveErr) => {
				if(saveErr){
					console.log(saveErr)
					return false
				} else {
					return updatedSpecificTask
				}
			})
	})
}
// ======================END OF ACTIVITY SOLUTION============================
