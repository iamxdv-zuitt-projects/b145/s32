const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({

	name : {
		type: String,
		required: [true, "Name of task is required."]
	},
	status : {
		type: String,
		default: 'pending'
	},
	createdOn : {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model('Task', taskSchema)
/* 
==================A C T I V I T Y============================
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the /tasks/:id route using postman to get a
specific task.
5. Create a route for changing the status of a task to complete.
6. Create a controller function for changing the status of a task to
complete.
7.
Return the result back to the client/Postman.
8. Process a PUT request at the /tasks/:id/complete route using
postman to update a task.
9. Create a git repository named $33.
10. Initialize a local git repository, add the remote link and push to git with
the commit message of Add activity code.
11. Add the link in Boodle.

==============================END================================
*/